#!/usr/bin/perl

use strict;

use warnings;

use Getopt::Long;

Getopt::Long::Configure ("bundling");

sub GetRandNum() {
  my $lower=33;
  my $upper=126;
  my $random = int(rand($upper - $lower+1)) + $lower;

  return $random;
}

sub DisplayUsage() {
  print "\nUsage: genpass [-OPTIONS] LENGTH\n";
  print "Generate a secure password LENGTH characters long.\n\n";
  print "\t-s, --symbols\t\tExclude symbol characters.\n";
  print "\t-n, --numbers\t\tExclude number characters.\n";
  print "\t-u, --uppercase\t\tExclude uppercase characters.\n";
  print "\t-l, --lowercase\t\tExclude lowercase characters.\n\n";
  print "\t--help\t\t\tDisplay this usage screen.\n\n";
}

my $symbols='0';		# all self-explanatory
my $numbers='0';
my $uppercase='0';
my $lowercase='0';
my $help='0';

GetOptions( 's|S|symbols' => \$symbols,
  'n|N|numbers' => \$numbers,
  'u|U|uppercase' => \$uppercase,
  'l|L|lowercase' => \$lowercase,
  'help' => \$help, );


my $kill='0';	# flag to stop the script if input is invalid (or --help is used)
my $errcount=0;	# count error messages
my @errmsg;	# error message array

if ($help eq '1') {
  DisplayUsage();
  $kill='1';

}

if (($symbols eq '1') && ($numbers eq '1') && ($uppercase eq '1') && ($lowercase eq '1')) {
  $errmsg[$errcount]="\ngenpass ERROR: At least 1 character-type must be included";
  $kill='1';
  $errcount++;
}

my $pwdlen=$ARGV[0];	# password length
my $count=0;		# a counter
my $password="";	# string to hold the password
my $isgood=0;		# validity flag
my $num=0;		# will hold random numbers

if ($#ARGV > 0 || $#ARGV < 0) {
  $errmsg[$errcount]="\ngenpass ERROR: Incorrect number of arguments passed";
  $kill='1';
  $errcount++;
} else {
  if($ARGV[0] !~ /^[0-9]+$/) {
    $errmsg[$errcount]="\ngenpass ERROR: Invalid input. LENGTH argument must be a numeric value";
    $kill='1';
    $errcount++;
  }
}

if ($kill eq '1' && $help eq '1') {		# If triggered by the --help option
  exit();
} elsif ($kill eq '1') {				 # If triggered by an error
  print "\n".$errcount." Error(s) found";  # show number of errors
  foreach my $err (@errmsg) {		 # display error messages
    print $err;
  }
  print "\nType genpass --help for command usage\n\n";
  exit();					 # exit script
}

while ($count < $pwdlen) {
  $isgood=0;		# reset the isgood flag
  $num=GetRandNum();	# get a random number
  if (($num >= 33 && $num <= 47) || ($num >= 58 && $num <= 64) || ($num >= 91 && $num <= 95) || ($num >= 123 && $num <= 126)) {
    if ($symbols eq '0') {	# symbols
      $isgood=1;
    }
  } elsif (($num >= 48 && $num <= 57)) {
    if ($numbers eq '0') {	# numbers
      $isgood=1;
    }
  } elsif (($num >= 65 && $num <= 90)) {
    if ($uppercase eq '0') { # uppercase
      $isgood=1;
    }
  } elsif (($num >= 97 && $num <= 122)) {
    if ($lowercase eq '0') { # lowercase
      $isgood=1;
    }
  }
  else {
    ; # do nothing
  }
  if ($isgood == 1) {
    $password=$password.chr($num);
    $count++;
  }
}

print $password."\n";
